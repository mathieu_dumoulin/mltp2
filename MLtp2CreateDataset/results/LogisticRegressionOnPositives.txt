-------------------------------------------------------
Testing with trainset size=50
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9178
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=100
-------------------------------------------------------
Final test Accuracy = 96.58
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
39335   312      |  39647   a     = 0
1381    8435     |  9816    b     = 1



Classification report:
AUC               : 0.9262
Positive precision: 85.93
Positive recall   : 96.43
Negative precision: 85.93
Negative recall   : 96.43
True negative rate: 96.61
Accuracy          : 96.58

-------------------------------------------------------
Testing with trainset size=150
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9175
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=200
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9176
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=250
-------------------------------------------------------
Final test Accuracy = 96.58
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
39335   312      |  39647   a     = 0
1381    8435     |  9816    b     = 1



Classification report:
AUC               : 0.9262
Positive precision: 85.93
Positive recall   : 96.43
Negative precision: 85.93
Negative recall   : 96.43
True negative rate: 96.61
Accuracy          : 96.58

-------------------------------------------------------
Testing with trainset size=300
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.6096
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=350
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.6095
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=400
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9135
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=450
-------------------------------------------------------
Final test Accuracy = 40.68
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10863   28784    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9179
Positive precision: 94.34
Positive recall   : 24.34
Negative precision: 94.34
Negative recall   : 24.34
True negative rate: 95.13
Accuracy          : 40.68

-------------------------------------------------------
Testing with trainset size=500
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9176
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=550
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9176
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71

-------------------------------------------------------
Testing with trainset size=600
-------------------------------------------------------
Final test Accuracy = 96.63
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
39364   283      |  39647   a     = 0
1382    8434     |  9816    b     = 1



Classification report:
AUC               : 0.9262
Positive precision: 85.92
Positive recall   : 96.75
Negative precision: 85.92
Negative recall   : 96.75
True negative rate: 96.61
Accuracy          : 96.63

-------------------------------------------------------
Testing with trainset size=650
-------------------------------------------------------
Final test Accuracy = 96.63
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
39364   283      |  39647   a     = 0
1382    8434     |  9816    b     = 1



Classification report:
AUC               : 0.9262
Positive precision: 85.92
Positive recall   : 96.75
Negative precision: 85.92
Negative recall   : 96.75
True negative rate: 96.61
Accuracy          : 96.63

-------------------------------------------------------
Testing with trainset size=700
-------------------------------------------------------
Final test Accuracy = 40.71
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
10875   28772    |  39647   a     = 0
556     9260     |  9816    b     = 1



Classification report:
AUC               : 0.9175
Positive precision: 94.34
Positive recall   : 24.35
Negative precision: 94.34
Negative recall   : 24.35
True negative rate: 95.14
Accuracy          : 40.71