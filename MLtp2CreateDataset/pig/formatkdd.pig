%default DATA_DIR ../data/kdd1999
%default DATA_FILE kddcup.data_10_percent.gz
%default OUTPUT_DIR ../data/kdd1999
%default PARALLEL_OPTION 3
%default POSITIVES_LIMIT 1000

SET default_parallel $PARALLEL_OPTION;

REGISTER 'lib/mltp2udf-2.0.jar';

DEFINE VECTORIZE_SERVICE ca.ulaval.ift7002.tp2.udf.VectorizeService();
DEFINE VECTORIZE_PROTOCOL_TYPE ca.ulaval.ift7002.tp2.udf.VectorizeProtocolType();
DEFINE VECTORIZE_FLAG ca.ulaval.ift7002.tp2.udf.VectorizeFlag();
DEFINE VECTORIZE_LABEL ca.ulaval.ift7002.tp2.udf.VectorizeBinaryLabel();

rmf $OUTPUT_DIR/train
rmf $OUTPUT_DIR/train-positives
rmf $OUTPUT_DIR/test
rmf $OUTPUT_DIR/unlabeled

kddData = LOAD '$DATA_DIR/$DATA_FILE' 
    USING PigStorage(',') 
    AS (duration:int, protocol_type:chararray, service:chararray, flag:chararray, 
    src_bytes:int,dst_bytes:int, land:chararray, wrong_fragment:int, 
    urgent:int, hot:int,num_failed_logins:int, logged_in:int, 
    num_compromised:int, root_shell: int, su_attempted:int, num_root:int, 
    num_file_creations: int, num_shells:int, num_access_files:int, 
    num_outbound_cmds:int, is_host_login:int, is_guest_login:int, 
    count:int, srv_count:int, serror_rate: double, srv_serror_rate: double, 
    rerror_rate: double, srv_rerror_rate: double, same_srv_rate: double, diff_srv_rate: double,
    srv_diff_host_rate: double, dst_host_count: double, dst_host_srv_count: double, 
    dst_host_same_srv_rate: double, dst_host_diff_srv_rate: double,
    dst_host_same_src_port_rate: double, dst_host_srv_diff_host_rate: double, 
    dst_host_serror_rate: double, dst_host_srv_serror_rate: double, 
    dst_host_rerror_rate: double, dst_host_srv_rerror_rate: double, label:chararray);

kdd_vectorized = FOREACH kddData GENERATE 
    VECTORIZE_LABEL(REPLACE(label, '\\.', '')) as label:int, 
    duration, VECTORIZE_PROTOCOL_TYPE(protocol_type) as protocol_type:int, 
    VECTORIZE_SERVICE(LOWER(service)) as service:int, VECTORIZE_FLAG(flag) as flag:int, 
    src_bytes,dst_bytes, land, wrong_fragment, 
    urgent, hot,num_failed_logins, logged_in, 
    num_compromised, root_shell , su_attempted, num_root, 
    num_file_creations , num_shells, num_access_files, 
    num_outbound_cmds, is_host_login, is_guest_login, 
    count, srv_count, serror_rate , srv_serror_rate , 
   rerror_rate , srv_rerror_rate , same_srv_rate , diff_srv_rate ,
    srv_diff_host_rate , dst_host_count , dst_host_srv_count , 
    dst_host_same_srv_rate , dst_host_diff_srv_rate ,
    dst_host_same_src_port_rate , dst_host_srv_diff_host_rate , 
    dst_host_serror_rate , dst_host_srv_serror_rate , 
    dst_host_rerror_rate , dst_host_srv_rerror_rate;

data = foreach kdd_vectorized generate *, RANDOM() as random;
data = order data by random;

split data into training if random <= 0.9, test if random > 0.9;
split training into train if random <= 0.1, unlabeled if random > 0.1;

positive_train = FILTER train BY label==1;
positive_train_filtered = LIMIT positive_train $POSITIVES_LIMIT;
 
train = FOREACH train GENERATE label..dst_host_srv_rerror_rate;
positive_train_filtered = FOREACH positive_train_filtered GENERATE label..dst_host_srv_rerror_rate;
unlabeled = FOREACH unlabeled GENERATE label..dst_host_srv_rerror_rate;
test = FOREACH test GENERATE label..dst_host_srv_rerror_rate;

STORE train INTO '$OUTPUT_DIR/train' USING PigStorage(',');
STORE positive_train_filtered INTO '$OUTPUT_DIR/train-positives' USING PigStorage(',');
STORE unlabeled INTO '$OUTPUT_DIR/unlabeled' USING PigStorage(',');
STORE test INTO '$OUTPUT_DIR/test' USING PigStorage(',');
