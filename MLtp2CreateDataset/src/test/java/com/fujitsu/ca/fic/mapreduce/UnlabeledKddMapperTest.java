package com.fujitsu.ca.fic.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.VectorWritable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class UnlabeledKddMapperTest {
    private final int FEATURES = 41;
    private String testFeatures;
    NamedVector expectedFeatures;

    @Mock
    private Mapper<LongWritable, Text, LongWritable, VectorWritable>.Context context;

    private MapDriver<LongWritable, Text, LongWritable, VectorWritable> mapDriver;
    private static LongWritable WRITABLE_ID = new LongWritable(1);

    @Before
    public void setUp() throws Exception {
        testFeatures = buildTestFeatureString();
        expectedFeatures = buildExpectedFeatures();
        mapDriver = MapDriver.newMapDriver(new UnlabeledKddMapper());
    }

    @Test
    public void testMap() throws IOException, InterruptedException {
        VectorWritable expected = new VectorWritable(new DenseVector(expectedFeatures));

        mapDriver.withInput(WRITABLE_ID, new Text(testFeatures));
        mapDriver.withOutput(WRITABLE_ID, expected);
        mapDriver.runTest();
    }

    private String buildTestFeatureString() {
        String test = "1.0";
        for (int i = 0; i < FEATURES; i++) {
            test = test + "," + String.valueOf(i) + ".0";
        }
        return test;
    }

    private NamedVector buildExpectedFeatures() {
        double[] expectedValues = new double[FEATURES];
        for (int i = 0; i < FEATURES; i++) {
            expectedValues[i] = i;
        }
        Vector expected = new SequentialAccessSparseVector(FEATURES);
        expected.assign(expectedValues);
        return new NamedVector(expected, "1.0");
    }
}
