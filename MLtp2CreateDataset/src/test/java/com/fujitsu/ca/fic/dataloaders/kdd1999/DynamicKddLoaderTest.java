package com.fujitsu.ca.fic.dataloaders.kdd1999;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.mahout.math.NamedVector;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.DynamicDatasetLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public class DynamicKddLoaderTest {
    private final static String TRAIN_PATH = "data/kdd1999/train";
    private final static String TEST_PATH = "data/kdd1999/test";
    private final static String TRAIN_POSITIVES_PATH = "data/kdd1999/train-positives";
    private final static String UNLABELED_PATH = "data/kdd1999/unlabeled";
    Configuration conf = new Configuration();

    @Test
    public void checkTrain3PartsOK() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, TRAIN_PATH);
        checkDatasetWithLoader(loader);
    }

    @Test
    public void checkTest1PartOK() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, TEST_PATH);
        checkDatasetWithLoader(loader);
    }

    @Test
    public void checkTrainPositives() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, TRAIN_POSITIVES_PATH);
        checkDatasetWithLoader(loader);
    }

    @Test
    public void checkUnlabeled() throws IOException {
        DynamicDatasetLoader loader = new DynamicKddLoader(conf, UNLABELED_PATH);
        checkDatasetWithLoader(loader);
    }

    private void checkDatasetWithLoader(DynamicDatasetLoader loader) {
        while (loader.hasNext()) {
            NamedVector nextExample;
            try {
                nextExample = loader.getNext();
                String labelName = nextExample.getName();
                int actual = Integer.parseInt(labelName);
                assertThat(actual, either(is(1)).or(is(0)));

            } catch (IncorrectLineFormatException e) {
                fail(e.getMessage());
            }
        }
    }
}
