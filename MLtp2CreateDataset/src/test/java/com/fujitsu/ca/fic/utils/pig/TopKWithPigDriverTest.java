package com.fujitsu.ca.fic.utils.pig;

import static org.hamcrest.MatcherAssert.*;
import static org.hamcrest.Matchers.*;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.math.NamedVector;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;

public class TopKWithPigDriverTest {
    static Configuration conf;

    @BeforeClass
    public static void setup() throws IOException {
        conf = new Configuration();
        conf.set("mapred.job.tracker", "local");
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-test-conf-1.xml")));
    }

    @Ignore
    public void fetchConfidentExamplesShouldNotThrowExceptions() {
        conf.set("topk.keep.ratio", "1");
        conf.set("topk.keep.limit", "10");
        conf.set("data.perceptron.output.path", "data/test/kdd1999/20vectors-with-confidence");
        try {
            TopKWithPigDriver driver = new TopKWithPigDriver(conf, new KddLoader());
            driver.fetchConfidentExamples();

        } catch (IOException e) {
            org.junit.Assert.fail("Exception thrown in fetchConfidentExamples: " + e.getMessage());
        }
    }

    @Test
    public void givenMax10AndConf02ShouldReturnListWithSize13() throws IOException {
        conf.set("topk.keep.ratio", "0.2");
        conf.set("topk.keep.limit", "10");
        conf.set("data.perceptron.output.path", "data/test/kdd1999/20vectors-with-confidence");

        TopKWithPigDriver driver = new TopKWithPigDriver(conf, new KddLoader());
        List<NamedVector> results = driver.fetchConfidentExamples();

        assertThat(results.size(), is(13));
    }

    @Test
    public void givenMax5AndConf02On50vectorsShouldReturnListWithSize10() throws IOException {
        conf.set("topk.keep.ratio", "0.2");
        conf.set("topk.keep.limit", "5");
        conf.set("data.perceptron.output.path", "data/test/kdd1999/50vectors-with-confidence");

        TopKWithPigDriver driver = new TopKWithPigDriver(conf, new KddLoader());
        List<NamedVector> results = driver.fetchConfidentExamples();

        assertThat(results.size(), is(10));
    }
}
