package com.fujitsu.ca.fic.mapreduce.jobs;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.math.NamedVector;
import org.junit.BeforeClass;
import org.junit.Test;

import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;

public class ClassifyWithPerceptronJobTest {
    private static final double GAMMA = 1;
    private static final double C = 1;
    static Configuration conf = new Configuration();
    Perceptron perceptron;
    AbstractDatasetFileLoader dataLoader = new KddLoader();

    @BeforeClass
    public static void setUp() throws Exception {
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-test-conf-1.xml")));
    }

    @Test
    public void configurationFileReadCorrectly() throws IOException {
        assertThat(conf.get("data.train-positives.path"), is("data/test/kdd1999/train-positives-small"));
        assertThat(conf.get("perceptron.model.path"), is("model/test/perceptron.model"));
        assertThat(conf.get("data.perceptron.output.path"), is("data/test/perceptron/output"));
    }

    @Test
    public void jobShouldNotThrowException() throws Exception {
        try {
            String trainPath = conf.get("data.train-positives-small.path");
            List<NamedVector> trainingExamples = dataLoader.load(conf, trainPath);

            perceptron = Perceptron.buildTrainedPerceptron(trainingExamples, C, GAMMA);
            perceptron.saveToFile(conf);

            String jobOutputPath = conf.get("data.perceptron.output.path");
            HadoopUtil.delete(conf, new Path(jobOutputPath));

            ClassifyWithPerceptronJob classifyWithPerceptronJob = new ClassifyWithPerceptronJob(conf);
            classifyWithPerceptronJob.scoreUnlabeledWithPerceptron();

        } catch (Exception e) {
            fail("Threw exception: " + e.getMessage());
        }
    }
}
