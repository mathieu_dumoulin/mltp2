package com.fujitsu.ca.fic.classifiers.perceptron;

import static org.junit.Assert.*;
import static org.mockito.Mockito.*;

import org.apache.mahout.math.Vector;
import org.junit.Before;
import org.junit.Test;

import com.fujitsu.ca.fic.classifiers.perceptron.RBFKernel;

public class RBFKernelTest {
    private final double A_GAMMA_VALUE = 0.5;
    private final int EXPONENT = 2;
    private final double POW_RESULT = 1;
    private final double SCALAR_PRODUCT = Math.exp(-0.5);
    private final double EPSILON_OFFSET = 0.000000001;

    private Vector firstVector;
    private Vector secondVector;
    private Vector minusVector;

    private RBFKernel kernel;

    @Before
    public void setup() {
        firstVector = mock(Vector.class);
        secondVector = mock(Vector.class);
        minusVector = mock(Vector.class);

        when(firstVector.minus(secondVector)).thenReturn(minusVector);
        when(minusVector.norm(EXPONENT)).thenReturn(POW_RESULT);

        kernel = new RBFKernel(A_GAMMA_VALUE);
    }

    @Test
    public void shouldReturnScalarProductAtAnytime() {
        double scalarProduct = kernel.calculateScalarProduct(firstVector, secondVector);

        assertEquals(SCALAR_PRODUCT, scalarProduct, EPSILON_OFFSET);
    }
}
