package utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

public class LocalCreate {
    // private static final String INPUT_PATH = "data/kdd1999/test/part-r-00000";
    // private static final String OUTPUT_PATH = "data/kdd1999/test/output.seq";
    private static final String INPUT_PATH = "data/10vectors.data";
    private static final String OUTPUT_PATH = "data/output.seq";
    private static final int FEATURES = 41;

    public static void main(String[] args) {
        File csvFile = new File(INPUT_PATH);
        try {
            List<NamedVector> vectorList = loadExamplesFromFile(csvFile);
            testSequenceFileWriteAndRead(vectorList);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<NamedVector> loadExamplesFromFile(File csvFile) throws IOException {
        List<NamedVector> vectorList = new ArrayList<NamedVector>();
        BufferedReader reader = new BufferedReader(new FileReader(csvFile));

        String line = reader.readLine();
        while (line != null && line.length() > 0) {
            vectorList.add(retrieveFeaturesFromLine(line));
            line = reader.readLine();
        }

        reader.close();
        return vectorList;
    }

    private static NamedVector retrieveFeaturesFromLine(String line) {
        String[] features = line.split(",");
        double[] featuresDouble = new double[FEATURES];

        for (int i = 0; i < FEATURES; i++) {
            featuresDouble[i] = Double.parseDouble(features[i + 1]);
        }

        Vector featureVector = new SequentialAccessSparseVector(FEATURES);
        featureVector.assign(featuresDouble);
        return new NamedVector(featureVector, features[0]);
    }

    private static void testSequenceFileWriteAndRead(List<NamedVector> vectorList) throws IOException {
        Configuration configuration = new Configuration();
        FileSystem fileSystem = FileSystem.get(configuration);
        Path outputPath = new Path(OUTPUT_PATH);
        if (fileSystem.exists(outputPath)) {
            fileSystem.delete(outputPath, true);
        }

        IOHelper.writeToFS(vectorList, outputPath, configuration);
        List<NamedVector> readVectors = IOHelper.readFromFS(outputPath, configuration);

        for (NamedVector v : readVectors) {
            System.out.println(v.toString());
        }

    }
}
