package utils;

import java.io.IOException;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileStatus;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.PathFilter;
import org.junit.Test;

public class HDFStest {

    @Test
    public void test() throws IOException {
        Configuration conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        FileStatus[] fileStatus = fs.listStatus(new Path("data/topk/output/negative"), new PathFilter() {
            @Override
            public boolean accept(Path path) {
                return path.getName().matches("part(.*)");
            }
        });

        for (FileStatus file : fileStatus) {
            System.out.println(file.getPath().toString());
        }
    }
}
