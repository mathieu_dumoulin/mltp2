package utils;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.Text;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.VectorWritable;

import com.google.common.collect.Lists;

public class IOHelper {
    public static void writeToFS(List<NamedVector> listToWrite, Path path, Configuration configuration) throws IOException {
        SequenceFile.Writer writer = new SequenceFile.Writer(FileSystem.get(configuration), configuration, path, Text.class,
                VectorWritable.class);

        VectorWritable vectorWritable = new VectorWritable();
        for (NamedVector vector : listToWrite) {
            vectorWritable.set(vector);
            writer.append(new Text(vector.getName()), vectorWritable);
        }
        writer.close();
    }

    public static List<NamedVector> readFromFS(Path path, Configuration configuration) throws IOException {
        SequenceFile.Reader reader = new SequenceFile.Reader(FileSystem.get(configuration), path, configuration);
        List<NamedVector> examples = Lists.newArrayList();
        Text key = new Text();
        VectorWritable value = new VectorWritable();
        while (reader.next(key, value)) {
            examples.add((NamedVector) value.get());
        }
        reader.close();

        return examples;
    }
}
