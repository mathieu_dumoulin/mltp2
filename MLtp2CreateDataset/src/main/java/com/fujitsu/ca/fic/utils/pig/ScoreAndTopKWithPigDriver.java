package com.fujitsu.ca.fic.utils.pig;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.math.NamedVector;
import org.apache.pig.ExecType;
import org.apache.pig.PigServer;
import org.apache.pig.backend.executionengine.ExecException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fujitsu.ca.fic.dataloaders.DatasetFileLoader;
import com.google.common.collect.Lists;

public class ScoreAndTopKWithPigDriver {
    private static Logger LOG = LoggerFactory.getLogger(ScoreAndTopKWithPigDriver.class);
    private final Map<String, String> params = new HashMap<String, String>();
    private final Configuration conf;
    private final DatasetFileLoader dataLoader;

    public ScoreAndTopKWithPigDriver(Configuration conf, DatasetFileLoader dataLoader) throws IOException {
        this.conf = conf;
        this.dataLoader = dataLoader;

        String topkOutputPath = conf.get("data.topk.output.path");
        if (topkOutputPath == null) {
            String message = "Output path for topk could not be read from Configuration!";
            LOG.error(message);
            throw new IOException(message);
        }
        HadoopUtil.delete(conf, new Path(topkOutputPath));

        params.put("KEEP_LIMIT", conf.get("topk.keep.limit"));
        params.put("KEEP_RATIO", conf.get("topk.keep.ratio"));
        params.put("DATA_DIR", conf.get("data.perceptron.output.path"));
        params.put("OUTPUT_DIR", topkOutputPath);
    }

    public List<NamedVector> fetchConfidentExamples() throws IOException {
        PigServer pigServer = null;
        try {
            pigServer = new PigServer(conf.get("mapred.job.tracker").equals("local") ? ExecType.LOCAL : ExecType.MAPREDUCE);
            pigServer.setJobName("topkwithpig");
            runScript(pigServer);

        } catch (ExecException e) {

        } finally {
            if (pigServer != null) {
                pigServer.shutdown();
            }
        }

        String outputDir = params.get("OUTPUT_DIR");
        List<NamedVector> confidentExamples = Lists.newArrayList();
        confidentExamples.addAll(dataLoader.load(conf, outputDir + "/positives"));
        confidentExamples.addAll(dataLoader.load(conf, outputDir + "/negatives"));
        return confidentExamples;
    }

    // TODO convert this to call the script from code instead of using register query!!!!
    // I can't get the parameters to fill in correctly right now :-(
    private void runScript(PigServer ps) throws IOException {
        ps.registerQuery("REGISTER 'lib/mltp2udf-2.0.jar';");
        ps.registerQuery("REGISTER 'lib/mahout-math-0.7.jar';");
        ps.registerQuery("REGISTER 'lib/mahout-core-0.7.jar';");

        ps.registerQuery("DEFINE SCORE_WITH_PERCEPTRON ca.ulaval.ift7002.tp2.udf.ScoreWithPerceptron();");
        ps.registerQuery("DEFINE VECTORIZE_SERVICE ca.ulaval.ift7002.tp2.udf.VectorizeService();");
        ps.registerQuery("DEFINE VECTORIZE_PROTOCOL_TYPE ca.ulaval.ift7002.tp2.udf.VectorizeProtocolType();");
        ps.registerQuery("DEFINE VECTORIZE_FLAG ca.ulaval.ift7002.tp2.udf.VectorizeFlag();");
        ps.registerQuery("DEFINE VECTORIZE_LABEL ca.ulaval.ift7002.tp2.udf.VectorizeBinaryLabel();");

        ps.registerQuery("kddData = LOAD '$DATA_DIR' USING PigStorage(',') "
                + "AS (label:int, duration:int, protocol_type:int, service:int, flag:int,"
                + "src_bytes:int,dst_bytes:int, land:int, wrong_fragment:int, "
                + "urgent:int, hot:int,num_failed_logins:int, logged_in:int, "
                + "num_compromised:int, root_shell: int, su_attempted:int, num_root:int,"
                + "num_file_creations: int, num_shells:int, num_access_files:int, "
                + "num_outbound_cmds:int, is_host_login:int, is_guest_login:int, "
                + "count:int, srv_count:int, serror_rate: double, srv_serror_rate: double,"
                + "rerror_rate: double, srv_rerror_rate: double, same_srv_rate: double, diff_srv_rate: double,"
                + "srv_diff_host_rate: double, dst_host_count: double, dst_host_srv_count: double, "
                + "dst_host_same_srv_rate: double, dst_host_diff_srv_rate: double,"
                + "dst_host_same_src_port_rate: double, dst_host_srv_diff_host_rate: double,"
                + "dst_host_serror_rate: double, dst_host_srv_serror_rate: double, "
                + "dst_host_rerror_rate: double, dst_host_srv_rerror_rate: double);");

        ps.registerQuery("kkdWithConfidence = FOREACH kddData GENERATE SCORE_WITH_PERCEPTRON(*) as confidence:double, $0..;");

        ps.registerQuery("data = LOAD '"
                + params.get("DATA_DIR")
                + "' USING PigStorage(',') AS (confidence:double, duration:int, protocol_type:chararray, service:chararray,"
                + "flag:chararray, src_bytes:int,dst_bytes:int, land:chararray, wrong_fragment:int, urgent:int, hot:int,num_failed_logins:int,"
                + "logged_in:int, num_compromised:int, root_shell: int, su_attempted:int, num_root:int, num_file_creations: int, num_shells:int,"
                + "num_access_files:int, num_outbound_cmds:int, is_host_login:int, is_guest_login:int, count:int, srv_count:int, serror_rate:"
                + "double, srv_serror_rate: double, rerror_rate: double, srv_rerror_rate: double, same_srv_rate: double, diff_srv_rate: double,"
                + "srv_diff_host_rate: double, dst_host_count: double, dst_host_srv_count: double, dst_host_same_srv_rate: double,"
                + "dst_host_diff_srv_rate: double, dst_host_same_src_port_rate: double, dst_host_srv_diff_host_rate: double, dst_host_serror_rate:"
                + "double, dst_host_srv_serror_rate: double, dst_host_rerror_rate: double, dst_host_srv_rerror_rate: double, label:chararray);"
                + "\n");
        ps.registerQuery("positives = FILTER data BY confidence > 0.0;" + "\n");
        ps.registerQuery("positivesGroup = GROUP positives ALL;" + "\n");
        ps.registerQuery("positivesCount = FOREACH positivesGroup GENERATE COUNT(positives);" + "\n");
        ps.registerQuery("positivesOrdered = ORDER positives by confidence DESC;" + "\n");

        ps.registerQuery("positivesLimited1 = LIMIT positivesOrdered (int) ((positivesCount.$0) * " + params.get("KEEP_RATIO") + " +1.0);"
                + "\n");
        ps.registerQuery("positivesLimited2 = LIMIT positivesLimited1 " + params.get("KEEP_LIMIT") + ";" + "\n");
        ps.registerQuery("positivesLimited = FOREACH positivesLimited2 GENERATE $1..;" + "\n");

        ps.registerQuery("negatives = FILTER data BY $0 <= 0.0;" + "\n");
        ps.registerQuery("negativesLimited = LIMIT negatives " + params.get("KEEP_LIMIT") + "; " + "\n");
        ps.registerQuery("negativesLimited = FOREACH negativesLimited GENERATE $1..;" + "\n");
        ps.registerQuery("STORE positivesLimited INTO '" + params.get("OUTPUT_DIR") + "/positives' USING PigStorage(',');" + "\n");
        ps.registerQuery("STORE negativesLimited INTO '" + params.get("OUTPUT_DIR") + "/negatives' USING PigStorage(',');" + "\n");
    }
}
