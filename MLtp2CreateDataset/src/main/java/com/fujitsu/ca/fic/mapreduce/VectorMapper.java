package com.fujitsu.ca.fic.mapreduce;

import java.io.IOException;
import java.util.regex.Pattern;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.VectorWritable;

public class VectorMapper extends Mapper<LongWritable, Text, LongWritable, VectorWritable> {
    private static int NB_FIELDS = 42;
    private VectorWritable writer;
    private Pattern splitter;

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String[] fields = splitter.split(value.toString());
        if (fields.length < NB_FIELDS) {
            context.getCounter("Map", "LinesWithErrors").increment(1);
            return;
        }

        NamedVector vector = parseFields(fields, key);
        writer.set(vector);
        context.write(key, writer);
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        splitter = Pattern.compile(",");
        writer = new VectorWritable();
    }

    private NamedVector parseFields(String[] fields, LongWritable key) {
        NamedVector vector = new NamedVector(new SequentialAccessSparseVector(NB_FIELDS), String.valueOf(key.get()));
        double[] features = new double[NB_FIELDS];
        int i = 0;
        for (String field : fields) {
            features[i++] = Double.parseDouble(field);
        }
        vector.assign(features);
        return vector;
    }
}
