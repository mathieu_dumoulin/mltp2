//package com.fujitsu.ca.fic.classifiers.drivers.extra;
//
//import java.io.IOException;
//import java.util.Collections;
//import java.util.List;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.conf.Configured;
//import org.apache.hadoop.util.Tool;
//import org.apache.hadoop.util.ToolRunner;
//import org.apache.mahout.classifier.sgd.CrossFoldLearner;
//import org.apache.mahout.classifier.sgd.L1;
//import org.apache.mahout.math.NamedVector;
//
//import com.fujitsu.ca.fic.classifiers.MahoutClassifierWrapper;
//import com.fujitsu.ca.fic.classifiers.metrics.ClassificationMetrics;
//import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
//import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
//import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
//import com.fujitsu.ca.fic.mapreduce.jobs.ClassifyWithPerceptronJob;
//import com.fujitsu.ca.fic.utils.Convert;
//import com.fujitsu.ca.fic.utils.pig.TopKWithPigDriver;
//import com.google.common.collect.Lists;
//
//public class ConfidentLearningAveragedPerformanceDriver extends Configured implements Tool {
//    private final static String TRAIN_POSITIVES_PATH = "data/kdd1999/train-positives";
//    private final static String TEST_PATH = "data/kdd1999/test/";
//    private final static String UNLABELED_PATH = "data/kdd1999/unlabeled/";
//
//    private static final String TOPK_OUTPUT_PATH = "data/topk/output";
//    private static final String PERCEPTRON_OUTPUT_PATH = "data/perceptron/output";
//
//    private static final double GAMMA = 0.55;
//    private static final double C = 1;
//    private static final int LAMBDA = 8;
//    private static final int FOLDS = 5;
//    private static final double CONFIDENCE_THRESHOLD = 0.2;
//    private static final int MAX_RESULTS = 2000;
//
//    public static void main(String[] args) throws Exception {
//        int exitCode = ToolRunner.run(new ConfidentLearningAveragedPerformanceDriver(), args);
//        System.exit(exitCode);
//    }
//
//    @Override
//    public int run(String[] arg0) throws Exception {
//        try {
//            Configuration conf = getConf();
//            AbstractDatasetFileLoader dataLoader = new KddLoader();
//            List<NamedVector> trainingExamples = dataLoader.load(conf, TRAIN_POSITIVES_PATH);
//
//            System.out.println("Training a RBF kernel perceptron on the positives-only training set");
//            Perceptron perceptron = new Perceptron(Convert.fromNamedVectorsToMatrix(trainingExamples, dataLoader.getFeaturesCount()), GAMMA);
//            perceptron.train(C);
//
//            System.out.println();
//            System.out.println("Classifying unlabeled with trained perceptron");
//            ClassifyWithPerceptronJob classifyWithPerceptronJob = new ClassifyWithPerceptronJob(conf, UNLABELED_PATH,
//                    PERCEPTRON_OUTPUT_PATH);
//            classifyWithPerceptronJob.applyConfidenceScoreToUnlabeled(perceptron);
//
//            List<MahoutClassifierWrapper> classificationRuns = Lists.newArrayList();
//            List<Integer> trainingSetSizes = Lists.newArrayList();
//            int RUNS = 10;
//            for (int i = 0; i < RUNS; i++) {
//                System.out.println();
//                System.out.println("Gather confident examples from unlabeled");
//                TopKWithPigDriver driver = new TopKWithPigDriver(conf, PERCEPTRON_OUTPUT_PATH, TOPK_OUTPUT_PATH, dataLoader);
//                List<NamedVector> newExamplesFromUnlabeled = driver.fetchConfidentExamples(CONFIDENCE_THRESHOLD, MAX_RESULTS);
//
//                System.out.println();
//                System.out.println("Run OnlineLogisticRegression on new training set and test it");
//                List<NamedVector> newTrainingSet = Lists.newArrayList();
//                newTrainingSet.addAll(trainingExamples);
//                newTrainingSet.addAll(newExamplesFromUnlabeled);
//
//                List<String> symbols = Lists.newArrayList("0", "1");
//                CrossFoldLearner crossFoldLearner = new CrossFoldLearner(FOLDS, dataLoader.getCategoriesCount(),
//                        dataLoader.getFeaturesCount(), new L1());
//                crossFoldLearner.stepOffset(1000).decayExponent(0.9).lambda(LAMBDA).learningRate(20);
//
//                MahoutClassifierWrapper classifyWithMahout = new MahoutClassifierWrapper(symbols);
//
//                Collections.shuffle(newTrainingSet);
//                classifyWithMahout.train(crossFoldLearner, newTrainingSet);
//                classifyWithMahout.test(crossFoldLearner, dataLoader.load(conf, TEST_PATH));
//
//                classificationRuns.add(classifyWithMahout);
//                trainingSetSizes.add(newTrainingSet.size());
//            }
//            double averagePrecision = 0;
//            double averageRecall = 0;
//            double averageAccuracy = 0;
//            for (MahoutClassifierWrapper mahoutClassifierWrapper : classificationRuns) {
//                ClassificationMetrics metrics = mahoutClassifierWrapper.getMetrics();
//                averageAccuracy += metrics.accuracy();
//                averageRecall += metrics.recall();
//                averagePrecision += metrics.precision();
//            }
//
//            System.out.println("Average precision: " + averagePrecision / RUNS);
//            System.out.println("Average recall: " + averageRecall / RUNS);
//            System.out.println("Average accuracy: " + averageAccuracy / RUNS);
//
//        } catch (IOException e) {
//            System.out.println(e.getMessage());
//            return -1;
//        }
//        return 0;
//    }
// }
