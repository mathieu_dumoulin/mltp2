package com.fujitsu.ca.fic.classifiers.drivers.extra;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.classifier.sgd.CrossFoldLearner;
import org.apache.mahout.classifier.sgd.L1;

import com.fujitsu.ca.fic.classifiers.MahoutClassifierWrapper;
import com.fujitsu.ca.fic.dataloaders.DatasetFileLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.DynamicKddLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
import com.google.common.collect.Lists;

public class ClassifyWithInputSizeOnPositivesDriver extends Configured implements Tool {
    private static final String POSITIVE_TRAIN_PATH = "data/kdd1999/train-positives";
    private static final String TEST_PATH = "data/kdd1999/test";
    private static List<String> SYMBOLS = Lists.newArrayList("0", "1");
    private static double LAMBDA = 85;
    private static final int FOLDS = 5;

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new ClassifyWithInputSizeOnPositivesDriver(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] arg0) throws IOException {
        Configuration conf = getConf();
        DatasetFileLoader dataLoader = new KddLoader();

        for (int trainSetSize = 50; trainSetSize < 750; trainSetSize += 50) {
            System.out.println("-------------------------------------------------------");
            System.out.println("Testing with trainset size=" + trainSetSize);
            System.out.println("-------------------------------------------------------");

            MahoutClassifierWrapper classifyWithMahout = new MahoutClassifierWrapper(SYMBOLS);
            CrossFoldLearner crossFoldLearner = new CrossFoldLearner(FOLDS, dataLoader.getCategoriesCount(), dataLoader.getFeaturesCount(),
                    new L1());
            crossFoldLearner.alpha(1).stepOffset(1000).decayExponent(0.9).lambda(LAMBDA).learningRate(20);

            classifyWithMahout.train(crossFoldLearner, new DynamicKddLoader(conf, POSITIVE_TRAIN_PATH), trainSetSize);

            classifyWithMahout.test(crossFoldLearner, new DynamicKddLoader(conf, TEST_PATH));
            classifyWithMahout.showClassificationReport();

            classifyWithMahout = null;
            crossFoldLearner = null;
            System.gc();
        }
        return 0;
    }
}
