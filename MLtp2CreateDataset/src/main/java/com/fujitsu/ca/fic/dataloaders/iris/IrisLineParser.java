package com.fujitsu.ca.fic.dataloaders.iris;

import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

import com.fujitsu.ca.fic.dataloaders.LineParser;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public class IrisLineParser implements LineParser {
    private final static int FEATURES = 4;
    private final static int LABEL_INDEX = 4;

    @Override
    public NamedVector parseFields(String line) throws IncorrectLineFormatException {
        double[] featuresDouble = new double[FEATURES];
        String[] features = line.split(",");
        try {
            for (int i = 0; i < FEATURES - 1; i++) {
                featuresDouble[i] = Double.parseDouble(features[i]);
            }
        } catch (Exception e) {
            throw new IncorrectLineFormatException(e.getMessage());
        }

        Vector featureVector = new SequentialAccessSparseVector(FEATURES);
        featureVector.assign(featuresDouble);
        return new NamedVector(featureVector, features[LABEL_INDEX]);
    }

}
