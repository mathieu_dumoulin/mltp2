package com.fujitsu.ca.fic.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.mahout.math.VectorWritable;

import com.fujitsu.ca.fic.dataloaders.LineParser;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLineParser;

public class UnlabeledKddMapper extends Mapper<LongWritable, Text, LongWritable, VectorWritable> {
    private LineParser parser;

    @Override
    protected void map(LongWritable key, Text value, Context context) throws IOException, InterruptedException {
        String line = value.toString();
        context.write(new LongWritable(1), new VectorWritable(parser.parseFields(line)));
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        parser = new KddLineParser();
    }
}
