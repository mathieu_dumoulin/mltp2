package com.fujitsu.ca.fic.mapreduce.jobs;

import java.io.IOException;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.mahout.common.HadoopUtil;
import org.apache.mahout.math.VectorWritable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fujitsu.ca.fic.mapreduce.FindConfidentExamplesReducer;
import com.fujitsu.ca.fic.mapreduce.UnlabeledKddMapper;

public class ClassifyWithPerceptronJob {
    private final Configuration conf;
    private static Logger LOG = LoggerFactory.getLogger(ClassifyWithPerceptronJob.class);

    public ClassifyWithPerceptronJob(Configuration conf) {
        this.conf = conf;
    }

    public void scoreUnlabeledWithPerceptron() throws IOException, InterruptedException, ClassNotFoundException {
        Path input = new Path(conf.get("data.unlabeled.path"));
        Path output = new Path(conf.get("data.perceptron.output.path"));
        if (input == null || output == null) {
            LOG.error("The input and/or ouput path could not be read from configuration!");
        }
        HadoopUtil.delete(conf, output);

        Job job = new Job(conf, "Compute Confidence of Unlabled with Perceptron");
        job.setJarByClass(getClass());

        job.setMapperClass(UnlabeledKddMapper.class);
        job.setReducerClass(FindConfidentExamplesReducer.class);

        job.setMapOutputKeyClass(LongWritable.class);
        job.setMapOutputValueClass(VectorWritable.class);
        job.setInputFormatClass(TextInputFormat.class);
        job.setOutputKeyClass(NullWritable.class);
        job.setOutputValueClass(Text.class);

        FileInputFormat.addInputPath(job, input);
        FileOutputFormat.setOutputPath(job, output);

        job.waitForCompletion(true);
    }
}
