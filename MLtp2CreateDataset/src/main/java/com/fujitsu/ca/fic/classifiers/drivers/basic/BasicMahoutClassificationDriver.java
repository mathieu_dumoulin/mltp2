package com.fujitsu.ca.fic.classifiers.drivers.basic;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.classifier.sgd.AdaptiveLogisticRegression;
import org.apache.mahout.classifier.sgd.CrossFoldLearner;
import org.apache.mahout.classifier.sgd.L1;

import com.fujitsu.ca.fic.classifiers.MahoutClassifierWrapper;
import com.fujitsu.ca.fic.dataloaders.kdd1999.DynamicKddLoader;
import com.google.common.collect.Lists;

/**
 * Launch a basic classification task using AdaptiveLogisticRegression and the DynamicDatasetLoader to train it one example at a time show
 * metrics of classification results on the console
 */
public class BasicMahoutClassificationDriver extends Configured implements Tool {
    private static List<String> SYMBOLS = Lists.newArrayList("0", "1");

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new BasicMahoutClassificationDriver(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] arg0) throws Exception {
        Configuration conf = getConf();
        if (conf.get("data.test.path") == null) {
            loadConfigurationFile(conf);
        }

        try {
            MahoutClassifierWrapper mahoutWrapper = new MahoutClassifierWrapper(SYMBOLS);

            System.out.println("-------------------------------------------");
            System.out.println("Classification with DynamicKddLoader");
            System.out.println("-------------------------------------------");
            String trainPath = conf.get("data.unlabeled.path");
            DynamicKddLoader trainDataLoader = new DynamicKddLoader(conf, trainPath);
            AdaptiveLogisticRegression adaptiveLearnerDyn = new AdaptiveLogisticRegression(trainDataLoader.getCategoriesCount(),
                    trainDataLoader.getFeaturesCount(), new L1());
            mahoutWrapper.train(adaptiveLearnerDyn, trainDataLoader);

            CrossFoldLearner bestModelDyn = adaptiveLearnerDyn.getBest().getPayload().getLearner();
            String testPath = conf.get("data.test.path");
            mahoutWrapper.test(bestModelDyn, new DynamicKddLoader(conf, testPath));
            mahoutWrapper.showClassificationReport();

            // TODO output parameters to System.out
            // mahoutWrapper.showClassifierParameters(bestModelDyn);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return 0;
    }

    private void loadConfigurationFile(Configuration conf) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-config.xml")));
    }

}
